<?php
/**
 * Created by PhpStorm.
 * User: mark
 * Date: 05-Mar-18
 * Time: 9:19 PM
 */

namespace WPezThemeChildBasic\App\Core\ArraysUtilities;


class ClassArraysUtilities {


    public static function insertArray( $arr_input = [], $arr_insert = [], $int_col = false ) {

        if ( $int_col === false ){
            $int_col = count($arr_input);
        }

        return array_merge(
            array_slice( (array) $arr_input, 0, $int_col, false ),
            (array) $arr_insert,
            array_slice( (array) $arr_input, $int_col, null, true )
        );

    }

    public static function insertArrayAssoc( $arr_input, $arr_insert, $int_col = false ) {

        if ( $int_col === false ){
            $int_col = count($arr_input);
        }

        return array_slice( (array) $arr_input, 0, $int_col , true ) +
               (array) $arr_insert +
               array_slice( (array) $arr_input, $int_col , null, true );

    }


    public static function mergeDeepArray( $args ) {

        $arr_args = func_get_args();

        return self::mergeDeepPrep( $arr_args );
    }

    public static function mergeDeepObject( $args ) {

        $arr_args = func_get_args();
        $bool_obj = true;

        return self::mergeDeepPrep( $arr_args, $bool_obj );
    }

    public static function mergeDeepObjectPlus( $args ) {

        $arr_args  = func_get_args();
        $bool_obj  = true;
        $bool_plus = true;

        return self::mergeDeepPrep( $arr_args, $bool_obj, $bool_plus );
    }

    /**
     * TODO - ?
     *
     * @param $args
     */
    protected static function mergeDeepPrep( $arr_args, $bool_obj = false, $bool_plus = false ) {

        //   $arr_args = func_get_args();

        $count   = count( $arr_args );
        $arr_ret = [];
        if ( $count == 1 ) {
            $plus    = [];
            $arr_ret = self::mergeDeepRecursive( $arr_args[0], $plus, $bool_obj, $bool_plus );
        } elseif ( $count > 1 ) {
            $arr_ret = self::mergeDeepRecursive( $arr_args[0], $arr_args[1], $bool_obj, $bool_plus );

            $x = 2;
            while ( $x < $count ) {
                $bool_obj = false;
                if ( is_object( $arr_args[ $x ] ) ) {
                    $bool_obj = true;
                }
                $arr_ret = self::mergeDeepRecursive( $arr_ret, $arr_args[ $x ], $bool_obj, $bool_plus );
                $x++;
            }
        }

        return $arr_ret;
    }


    /**
     * As inspired by:
     * https://mekshq.com/recursive-wp-parse-args-wordpress-function/
     *
     * @param      $base
     * @param      $plus
     * @param bool $bool_obj
     * @param bool $bool_plus
     *
     * @return array|object
     */
    protected static function mergeDeepRecursive( $base, &$plus, $bool_obj = false, $bool_plus = false ) {

        $bool_obj_it = false;

        if ( $bool_obj === true && is_object( $base ) ) {
            $bool_obj_it = true;
        } elseif ( $bool_plus === true && ( is_object( $base ) || is_object( $plus ) ) ) {
            $bool_obj_it = true;
        }

        $base   = (array)$base;
        $plus   = (array)$plus;
        $result = $base;
        foreach ( $plus as $k => &$v ) {
            if ( ( is_array( $v ) || is_object( $v ) ) && isset( $result[ $k ] ) ) {

                $result[ $k ] = self::mergeDeepRecursive( $result[ $k ], $v, $bool_obj, $bool_plus );
            } else {
                $result[ $k ] = $v;
            }
        }
        if ( $bool_obj_it ) {
            return (object)$result;
        }

        return $result;
    }
}
