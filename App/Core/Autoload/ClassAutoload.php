<?php

namespace WPezThemeChildBasic;

if ( ! defined( 'ABSPATH' ) ) {
    header( 'HTTP/1.0 403 Forbidden' );
    die();
}

class ClassAutoload {

    protected $_str_needle_root ;
    protected $_str_needle_child;
    protected $_str_path_base;
    protected $_str_remove_from_class;

    private $_version;
    private $_url;
    private $_path;
    private $_path_parent;
    private $_basename;
    private $_file;


    public function __construct( $arr_args = [] ) {

        $this->setup();

        $this->setPropertyDefaults();

        spl_autoload_register( null, false );

    }

    /**
     *
     */
    protected function setup() {

        $this->_version     = '0.5.0';
        $this->_url         = plugin_dir_url( __FILE__ );
        $this->_path        = plugin_dir_path( __FILE__ );
        $this->_path_parent = dirname( $this->_path );
        $this->_basename    = plugin_basename( __FILE__ );
        $this->_file        = __FILE__;
    }

    protected function setPropertyDefaults() {

        $this->_str_needle_root  = __NAMESPACE__;
        $this->_str_needle_child = false;
        $this->_str_path_base = false;
        $this->_str_remove_from_class = __NAMESPACE__;
    }

    public function setNeedleRoot( $str ) {

        $this->_str_needle_root = $str;
        //
        $this->_str_remove_from_class = $str;
    }

    public function setNeedleChild( $str ) {

        $this->_str_needle_child = $str;
    }

    public function setPathBase( $str ) {

        $this->_str_path_base = $str;
    }

    public function setRemoveFromClass( $str){

        $this->_str_remove_from_class = $str;
    }


    /**
     * (@link http://www.phpro.org/tutorials/SPL-Autoload.html)
     *
     * The classes naming convention allows us to parse the folder structure
     * out of the class / file name. And then use that to define the $file for
     * the require_once()
     */
    public function wpezAutoload( $str_class ) {

        if ( strrpos( $str_class, $this->_str_needle_root, -strlen( $str_class ) ) === false ) {

            return false;
        }

        $arr_class = explode( '\\', $str_class );

        if ( count($arr_class) < 3 || $this->_str_needle_child === false || ! isset( $arr_class[1] ) || $arr_class[1] != $this->_str_needle_child ) {

            return false;
        }

        $str_file = implode( DIRECTORY_SEPARATOR, $arr_class );
        $str_file = ltrim(str_replace($this->_str_remove_from_class, '', $str_file), DIRECTORY_SEPARATOR);
        $str_file = rtrim($this->_str_path_base,"/")  .DIRECTORY_SEPARATOR . $str_file . '.php';

        if ( file_exists( $str_file ) ) {

            require( $str_file );

            return true;
        }

        return false;
    }

}