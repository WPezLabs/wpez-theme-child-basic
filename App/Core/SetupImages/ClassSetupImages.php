<?php

namespace WPezThemeChildBasic\App\Core\SetupImages;

use WPezThemeChildBasic\App\Core\ArraysUtilities\ClassArraysUtilities as ArraysUtils;

class ClassSetupImages {


    protected $_int_jpg_quality;
    protected $_arr_image_sizes;
    protected $_str_post_thumbnail_size_name;
    protected $_mix_post_thumbnail_size_crop;
    protected $_int_content_width_count;
    protected $_int_content_width;
    protected $_int_content_width_temp;
    protected $_bool_content_width_compare;
    protected $_str_names_choose_add_where;
    protected $_arr_image_size_defaults;


    public function __construct() {

        $this->setPropertyDefaults();


    }

    protected function setPropertyDefaults() {

        $this->_int_jpg_quality              = 82;
        $this->_arr_image_sizes              = [];
        $this->_str_post_thumbnail_size_name = false;
        $this->_mix_post_thumbnail_size_crop = false;
        $this->_int_content_width_count      = 0;
        $this->_int_content_width            = 0;
        $this->_int_content_width_temp       = false;
        $this->_bool_content_width_compare   = false;
        $this->_str_names_choose_add_where   = 'before';
        $this->_arr_image_size_defaults      = (object)[
            'active'       => true,
            'name'         => false,
            'width'        => 0,
            'height'       => 0,
            'crop'         => false,
            'names_choose' => (object)[
                'active'                         => true,
                'option'                         => false,
                'content_width_compare_override' => false
            ]
        ];

    }

    public function setJpgQuality( $int = 82 ) {

        $this->_int_jpg_quality = absint( $int );
    }


    public function jpgQualityFilter() {

        return $this->_int_jpg_quality;

    }


    /**
     * We actually override the WP default and default to int_content_width =
     * 0.
     *
     * Ref: https://codex.wordpress.org/Content_Width
     * Ref:
     * https://wycks.wordpress.com/2013/02/14/why-the-content_width-wordpress-global-kinda-sucks/
     *
     * @param int $int
     */
    public function setContentWidth( $int = false ) {

        // using the method but not passing a value will use the global $content_width value
        if ( $int === false ) {
            $this->_int_content_width = false;

        } else {
            $this->_int_content_width = absint( $int );
        }
    }


    /**
     * Note: If set to true, any *custom* (read: add_image_size) image with a
     * width > $content_width will NOT be added via the image_size_names_choose
     * filter. The default setting of false will not do any sort of checking.
     *
     * @param bool $bool
     */
    public function setContentWidthCompare( $bool = false ) {

        $this->_bool_content_width_compare = (bool)$bool;
    }


    public function updateImageSizeDefaults( $obj = false ) {

        if ( is_object( $obj ) ) {
            $this->_arr_image_size_defaults = ArraysUtils::mergeDeepObject( (array)$this->_arr_image_size_defaults, (array)$obj );
        }
    }

    protected function setNamesChooseActive( $obj_names_choose ) {

        if ( $obj_names_choose->option === false || empty( esc_attr( $obj_names_choose->option ) ) ) {

            return false;
        }

        return true;
    }

    /**
     * Add a single image size definition
     *
     * @param array $arr_image_size
     */
    public function pushImageSize( $arr_image_size = [] ) {

        if ( is_array( $arr_image_size ) && ! empty( $arr_image_size ) ) {

            $obj_temp = ArraysUtils::mergeDeepObject( $this->_arr_image_size_defaults, $arr_image_size );
            if ( $obj_temp->active !== false && is_string( $obj_temp->name ) ) {

                $obj_temp->names_choose->active = $this->setNamesChooseActive( $obj_temp->names_choose );
                $this->_arr_image_sizes[]       = $obj_temp;
            }

        }
    }

    /**
     * Add an array
     *
     * @param array $arr
     */
    public function setImageSizes( $arr = [] ) {

        $arr_image_sizes = [];

        if ( is_array( $arr ) ) {
            foreach ( $arr as $key => $arr_image_size ) {

                $this->pushImageSize( $arr_image_size );

            }
        }
    }

    public function addImageSize() {

        foreach ( $this->_arr_image_sizes as $key => $obj_image_size ) {

            if ( $obj_image_size->active !== false ) {

                // add_image_size( string $name, int $width, int $height, bool|array $crop = false )
                // https://developer.wordpress.org/reference/functions/add_image_size/
                add_image_size(
                    $obj_image_size->name,
                    $obj_image_size->width,
                    $obj_image_size->height,
                    $obj_image_size->crop
                );
            }
        }
    }

    public function setPostThumbnailSizeName( $str_image_size_name = false ) {

        if ( is_string( $str_image_size_name ) ) {

            $this->_str_post_thumbnail_size_name = $str_image_size_name;
        }
    }

    public function setPostThumbnailSizeCrop( $mix_crop = false ) {

        if ( is_bool( $mix_crop ) || is_array( $mix_crop ) ) {

            $this->_mix_post_thumbnail_size_crop = $mix_crop;
        }
    }


    public function setPostThumbnailSize() {

        foreach ( $this->_arr_image_sizes as $key => $obj_image_size ) {

            if ( $obj_image_size->name == $this->_str_post_thumbnail_size_name ) {

                set_post_thumbnail_size( $obj_image_size->width, $obj_image_size->height, $this->_mix_post_thumbnail_size_crop );
                break;
            }
        }
    }

    public function setImageSizeNamesChooseAddHow( $str = 'after' ) {

        $this->_str_names_choose_add_where = $str;

    }

    public function imageSizeNamesChooseFilter( $sizes ) {

        // ref: https://wycks.wordpress.com/2013/02/14/why-the-content_width-wordpress-global-kinda-sucks/

        $arr_sizes = apply_filters( 'imageSizeNamesChooseFilterCustomizeSizes', $sizes );

        if ( is_array( $arr_sizes ) ) {
            $sizes = $arr_sizes;
        }

        //var_dump($sizes)

        $arr_names_choose = [];

        // we want this - at least for now - only for the first time this filter fires
        if ( $this->_int_content_width_count == 0 ) {

            $this->_int_content_width_count++;

            global $content_width;
            $this->_int_content_width_temp = $content_width;
            if ( ! is_bool( $this->_int_content_width ) ) {
                $content_width = $this->_int_content_width;
            }

            foreach ( $this->_arr_image_sizes as $key => $obj_image_size ) {

                // active?
                if ( $obj_image_size->active !== false && $obj_image_size->names_choose->active !== false ) {
                    // do we want to add this one or not?
                    if ( $obj_image_size->width > $content_width && $this->_bool_content_width_compare === true && $obj_image_size->names_choose->content_width_compare_override !== true ) {
                        continue;
                    }
                    $arr_names_choose[ $obj_image_size->name ] = $obj_image_size->names_choose->option;
                }
            }

        } else {

            // flip the content width back to what it was for anything downstream
            if ( $this->_int_content_width_temp !== false ) {
                global $content_width;
                $content_width = $this->_int_content_width_temp;
            }

        }

        $arr_ret = [];
        switch ( $this->_str_names_choose_add_where ) {

            case 'before':
                $arr_ret = array_merge( $arr_names_choose, $sizes );
                break;

            case 'replace':
                $arr_ret = $arr_names_choose;
                break;

            case 'after':
            default:
                $arr_ret = array_merge( $sizes, $arr_names_choose );
                break;
        }

        return $arr_ret;
    }


}