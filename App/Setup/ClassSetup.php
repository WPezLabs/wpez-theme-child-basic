<?php

namespace WPezThemeChildBasic\App\Setup;

use WPezThemeChildBasic\App\Core\HooksRegister\ClassHooksRegister;
use WPezThemeChildBasic\App\Core\SetupImages\ClassSetupImages;
use WPezThemeChildBasic\App\Setup\Styles\ClassStyles;

class ClassSetup {

    protected $_new_reg_hooks;

    protected $_arr_actions;

    protected $_arr_filters;


    public function __construct() {

        $this->setPropertyDefaults();

        $this->images( true );

        $this->styles();

        // this should be last
        $this->registerHooks();
    }

    protected function setPropertyDefaults() {

        $this->_new_reg_hooks = new ClassHooksRegister();
        $this->_arr_actions   = [];
        $this->_arr_filters   = [];
    }

    /**
     * After gathering (below) the arr_actions and arr_filter, it's time to
     * make some RegisterHook magic
     */
    protected function registerHooks() {

        $this->_new_reg_hooks->loadActions( $this->_arr_actions );

        $this->_new_reg_hooks->loadFilters( $this->_arr_filters );

        $this->_new_reg_hooks->doRegister();

    }


    protected function images( $bool = true ) {

        if ( $bool !== true ) {
            return;
        };
        // what
        $new_images = new ClassSetupImages();

        $arr_ais = [];

        /**
         * Note: These sizes are based on Bootstrap 4 breakpoints
         */
        $arr_ais[] = [
            'active'       => 'true',
            'name'         => 'wpez_xs',
            'width'        => 576,
            'height'       => 576,
            'crop'         => false,
            'names_choose' => [
                'active' => true,
                'option' => 'XS',
            ]
        ];

        $arr_ais[] = [
            'active'       => true,
            'name'         => 'wpez_sm',
            'width'        => 768,
            'height'       => 768,
            'crop'         => false,
            'names_choose' => [
                'active' => true,
                'option' => 'SM ',
            ]
        ];

        $arr_ais[] = [
            'active'       => true,
            'name'         => 'wpez_md',
            'width'        => 992,
            'height'       => 992,
            'crop'         => false,
            'names_choose' => [
                'active' => true,
                'option' => 'MD',
            ]
        ];

        $arr_ais[] = [
            'active'       => true,
            'name'         => 'wpez_lg',
            'width'        => 1200,
            'height'       => 1200,
            'crop'         => false,
            'names_choose' => [
                'active' => true,
                'option' => 'LG',
            ]
        ];

        $arr_ais[] = [
            'active'       => true,
            'name'         => 'wpez_xl',
            'width'        => 1500,
            'height'       => 1500,
            'crop'         => false,
            'names_choose' => [
                'active' => true,
                'option' => 'XL',
            ]
        ];

        // - jpgQualityFilter
        // - https://developer.wordpress.org/reference/hooks/jpeg_quality-2/
        // - https://developer.wordpress.org/reference/hooks/wp_editor_set_quality/
        /*
        $int_jpg_quality = 82;
        $new_images->setJpgQuality($int_jpg_quality);
        */


        // add_filter( 'jpg_quality', [ $new_images, 'jpgQualityFilter' ] );
        /*
        $this->_arr_filters[] = [

            'active'    => true,
            'hook'      => 'jpg_quality',
            'component' => $new_images,
            'callback'  => 'jpgQualityFilter'
        ];

        // add_filter( 'wp_editor_set_quality', [ $new_images, ' jpgQualityFilter' ] );
        $this->_arr_filters[] = [

            'active'    => true,
            'hook'      => 'wp_editor_set_quality',
            'component' => $new_images,
            'callback'  => 'jpgQualityFilter'
        ];
        */

        $new_images->setImageSizes( $arr_ais );


        // - imageSizeNamesChooseFilter
        // - Perhaps you want to unset() and/or rename the $sizes? This is your chance :)
        // - note: if you don't return and array[] your changes will be ignored. $sizes has to be an []

        // add_filter('imageSizeNamesChooseFilterCustomizeSizes', function($sizes){ return $sizes; });

        // - imageSizeNamesChooseFilter
        // - *temporarily set the content_width for the names_choose  filter
        // - 0 (zero) tells WP "ignore your content_width check for the names_choose"
        // ref: https://codex.wordpress.org/Content_Width

        // $new_images->setContentWidth();


        // - imageSizeNamesChooseFilter
        // - true will compare the image width to the content_width and stop names_choose for images with widths wider than the content_width

         //$bool_width_compare = true;
         // $new_images->setContentWidthCompare($bool_width_compare);


        // - within the imageSizeNamesChooseFilter
        // - when adding these new image sizes to the names_choose $sizes array: 'before', 'after' or 'replace'
        /*
        $str_add_how = 'after';
        $new_images->setImageSizeNamesChooseAddWhere($str_add_how);
        */

        // - setPostThumbnailSize
        // - use set_post_thumbnail_size() by using the name of one of your custom image sizes
        // - note: the name needs to be one of the custom image sizes (above), and not one of the native WP image sizes
        // - ref: https://developer.wordpress.org/reference/functions/set_post_thumbnail_size/
        /*
        $str_img_size_name = 'TODO';
        $new_images->setPostThumbnailSizeName($str_img_size_name);

        // $mix_crop = false; // bool or array
        // $new_images->setPostThumbnailSizeCrop($mix_crop);

        */

        // add_action( 'init', [ $new_images, 'setPostThumbnailSize' ] );
        $this->_arr_actions[] = [

            'active'    => false, //   <<<
            'hook'      => 'init',
            'component' => $new_images,
            'callback'  => 'setPostThumbnailSize'
        ];

        // when
        // add_action( 'after_setup_theme', [ $new_images, 'addImageSize' ] );
        $this->_arr_actions[] = [

            'active'    => true, //   <<<
            'hook'      => 'after_setup_theme',
            'component' => $new_images,
            'callback'  => 'addImageSize'
        ];

        // add_filter( 'image_size_names_choose', [ $new_images, 'imageSizeNamesChooseFilter' ], 100, 1 );
        $this->_arr_filters[] = [

            'active'    => true, //   <<<
            'hook'      => 'image_size_names_choose',
            'component' => $new_images,
            'callback'  => 'imageSizeNamesChooseFilter',
            'priority'  => 100
        ];

    }


    protected function styles( $bool = true ) {

        if ( $bool !== true ) {
            return;
        }

        // what
        $new_styles = new ClassStyles();

        // add_action( 'wp_enqueue_scripts', [ $new_styles, 'wpEnqueueStyleParent' ] );
        $this->_arr_actions[] = [

            'active'    => false, //   <<<
            'hook'      => 'wp_enqueue_scripts',
            'component' => $new_styles,
            'callback'  => 'wpEnqueueStyleParent'
        ];

        // add_action( 'wp_enqueue_scripts', [ $new_styles, 'wpEnqueueStyleChild' ], 10 );
        $this->_arr_actions[] = [

            'active'    => true, //   <<<
            'hook'      => 'wp_enqueue_scripts',
            'component' => $new_styles,
            'callback'  => 'wpEnqueueStyleChild'
        ];

    }


}