<?php

namespace WPezThemeChildBasic\App\Setup\Styles;


class ClassStyles {

    protected $_str_parent_style = 'parent-style';
    // Update the ver_ anytime the parent theme is updated
    protected $_str_parent_style_ver = 'v1.4';
    protected $_str_child_style      = 'main-style';
    protected $_str_child_style_ver = 'v20180101.1';

    public function __construct() {
    }

    public function wpEnqueueStyleParent() {

        wp_enqueue_style(
            $this->_str_parent_style,
            get_template_directory_uri() . '/style.css',
            [],
            $this->_str_parent_style_ver );

    }


    public function wpEnqueueStyleChild() {

        wp_enqueue_style(
            $this->_str_child_style,
            get_stylesheet_directory_uri() . '/app/assets/dist/css/main.min.css',
            [ $this->_str_parent_style ],
            $this->_str_child_style_ver );
    }



}