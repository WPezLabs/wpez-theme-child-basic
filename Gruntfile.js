module.exports = function(grunt) {

    'use strict';

    // Project configuration.
    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),

        options: {
            css: 'scss'		// .ext of src css files
        },

        imagemin: {
            options: {
                optimizationLevel: 7
            },
            dynamic: {
                files: [{
                    expand: true,
                    cwd: '<%= gruntConfig.IMG.paths.src %>',
                    src: ['**/*.{png,jpg,jpeg,gif}'],
                    dest: '<%= gruntConfig.IMG.paths.dist %>'
                }]
            }
        },

        sass:{
            css: {
                options: {
                    style: 'expanded'
                },
                files: {
                    // 'destination' : 'source'
                    '<%= gruntConfig.CSS.paths.build %>main.css' : '<%= gruntConfig.CSS.paths.src %>main.scss'
                }
            }
        },

        postcss: {
            options: {
                processors: [
                    // add vendor prefixes
                    require('autoprefixer')({browsers: 'last 3 versions'})
                 //   require('cssnano')() // minify the result
                ]
            },
            build: {
                expand:true,
                src: '<%= gruntConfig.CSS.paths.build %>*.css'
            }
        },

        cssmin: {
            options: {
                mergeIntoShorthands: false,
                roundingPrecision: -1
            },
            finish: {
                files: [{
                    expand: true,
                    cwd: '<%= gruntConfig.CSS.paths.build %>',
                    src: ['*.css', '!*.min.css'],
                    dest: '<%= gruntConfig.CSS.paths.dist %>',  // all done...move it to dist/
                    ext: '.min.css'
                }]
            }
        },

        watch: {

            configFiles: {
                files: [ 'Gruntfile.js' ],
                options: {
                    reload: true
                }
            },

            css: {
                files: [
                    '<%= gruntConfig.CSS.paths.src %>*.scss',
                    '<%= gruntConfig.CSS.paths.src + "/**/" %>*.scss'
                ],
                tasks: ['sass:css', 'postcss:build', 'cssmin:finish'],
                options: {
                    spawn: false,
                    livereload: true
                }
            },

            js: {
                files: [
                    '<%= gruntConfig.JS.paths.build %>*.js'
                ],
                tasks: ['uglify'],
                options: {
                    spawn: false,
                    livereload: true
                }
            }
        }
    });

    // https://www.npmjs.com/package/grunt-contrib-imagemin
    grunt.loadNpmTasks('grunt-contrib-imagemin');

    // WATCH - Run predefined tasks whenever watched file patterns are added, changed or deleted.
    // https://www.npmjs.com/package/grunt-contrib-watch
    grunt.loadNpmTasks('grunt-contrib-watch');

    // SASS - Compile Sass to CSS
    // https://www.npmjs.com/package/grunt-sass
    // NOTE: Win 10 had issues with:  grunt.loadNpmTasks('grunt-contrib-sass'); https://www.npmjs.com/package/grunt-contrib-sass
    grunt.loadNpmTasks('grunt-sass');

    // POSTCSS - Apply several post-processors to your CSS using PostCSS
    // https://www.npmjs.com/package/grunt-postcss
    grunt.loadNpmTasks('grunt-postcss');

    // MINCSS - Minify CSS.
    // https://www.npmjs.com/package/grunt-contrib-cssmin
    grunt.loadNpmTasks('grunt-contrib-cssmin');

    // JS HINT - Validate files with JSHint
    // https://www.npmjs.com/package/grunt-contrib-jshint
    grunt.loadNpmTasks('grunt-contrib-jshint');

    // CONCAT - oncatenate files.
    // https://www.npmjs.com/package/grunt-contrib-concat
    grunt.loadNpmTasks('grunt-contrib-concat');

    // UGILIFY (JS only) - Minify files with UglifyJS
    // https://www.npmjs.com/package/grunt-contrib-uglify
    grunt.loadNpmTasks('grunt-contrib-uglify');

    // Default task(s).
    grunt.registerTask('default', ['imagemin', 'watch']);

    grunt.registerTask('js', function(){
    	grunt.task.run(['concat', 'uglify']);
    });

    var grntConfig = {

        CSS:{
            "paths":{
                "src": "app/assets/src/css/scss/",
                "build": "app/assets/src/css/build/",
                "dist": "app/assets/dist/css/"
            }
        },

        JS:{
            "paths": {
                "src": "app/assets/src/js/",
                "build": "app/assets/src/js/build/",
                "dist": "app/assets/dist/js/"
            }
        },

        IMG:{
            "paths": {
                "src": "app/assets/src/img/",
                "build": "app/assets/src/img/build/",
                "dist": "app/assets/dist/img/"
            }
        }
    };

    grunt.config.set('gruntConfig', grntConfig);
};