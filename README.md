## WPezThemeChildBasic

__WordPress child theme boilerplate__

_No frills. No extra special ez magic**_

> --
>
> Special thanks to JetBrains (https://www.jetbrains.com/) and PhpStorm (https://www.jetbrains.com/phpstorm/) for their support of OSS and its devotees. 
>
> --


### OVERVIEW

- Logical ez to follow folder structure
- Grunt boilerplate for SCSS and js pre-processing

> --
>
> For customization of the base parent theme before you extend it with a child use this:
>
> https://gitlab.com/WPezPlugins/wpez-theme-customize
>
> This functionality was removed from this project as of 0.0.5.
>
> --


### GETTING STARTED 

1. Rename project folder
2. Update syle.css in the project root
3. Grunt - https://gruntjs.com/getting-started > Working with an existing Grunt project


-----------------------------

### TODO

- Finish other App / Setup classes, a la ClassImages.php
- There's a "stray" method in ClassStyles that should be moved elsewhere. 


### CHANGE LOG


__-- 0.0.9.3__

- FIXED - Gruntfile.js to watch new src css folders
- FIXED - Typo in _variables.scss


__-- 0.0.9.2__

- CHANGED - Removed the hook in Setup to deenqueue a parent script. That's now done via the WPezThemeCustomize plugin. 

__-- 0.0.9.1__

- CHANGED - Adopted 7-1 architecture pattern for SCSS (https://github.com/HugoGiraudel/sass-boilerplate)
- CHANGED - Various (minor) updates results from 7-1 adoption

__-- 0.0.9__

- CHANGED - Refactored SetupImages
- CHANGED - Refactored HooksRegister
- CHANGED - Began to migrate Setup to its new home under App / Core
- CHANGED - Refactorying due to flatten of Core folder structure
- UPDATED - package.json

__-- 0.0.8__

- CHANGED - Restructed Core/RegisterHooks to Core/Hooks/Register

__-- 0.0.7__

- ADDED - ClassRegisterHooks
- CHANGED - Refactoring to bring it up to ez (code style) speed
- CHANGED - Numerous minor nips, tucks, etc. based on the above

__-- 0.0.6__

- ADDED - Folder deploy-ignore for "project stuff" that deserves committing. e.g., Moved Pixlr.com template for screenshot.* to this folder
- CHANGED - Major upgrade / refactoring of ClassImages.php (in App / )Setup / Images)
- ADDED - Folder App / Core for (truth me told) classes and such that are normally WPezClasses. This project is intended to be dependency-free
- ADDED - App / Core / Autoload / ClassAutoload.php
- ADDED - App / Core / Utilities / ClassStaticUtilities.php 
- CHANGED - Above led to updates to functions.php, as well as ClassSetup.php
- UPDATED - README

-----------------------------




** Note: It's anticipated that there will eventually be a proper child theme boilerplate that incorporates more of "The ezWay." WPezChildBasic is intentionally less opinionated and for use with any parent theme.