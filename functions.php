<?php

namespace WPezThemeChildBasic;

use WPezThemeChildBasic\App\Setup\ClassSetup;

function autoloader( $bool = true ) {

    if ( $bool !== true ) {
        return;
    }

    include 'App/Core/Autoload/ClassAutoload.php';
    $new_autoload = new ClassAutoload();

    $new_autoload->setNeedleRoot( __NAMESPACE__ );
    $new_autoload->setNeedleChild( 'App' );
    $new_autoload->setPathBase( plugin_dir_path( __FILE__ ) );

    spl_autoload_register( [$new_autoload, 'wpezAutoload'], true );

}
autoloader();

function setup( $bool = true ) {

    if ( $bool !== true ) {
        return;
    }

    $new_setup = new ClassSetup();
    // TODO - add filter?
}
setup();

